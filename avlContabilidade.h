#ifndef AVL_C
#define AVL_C


typedef struct TAVL* COAVL;


int height_contabilidade(COAVL N);
int max_contabilidade(int a, int b);
COAVL newNode_contabilidade (char * id);
COAVL rightRotate_contabilidade(COAVL y);
COAVL leftRotate_contabilidade(COAVL x);
int getBalance_contabilidade(COAVL N);
int procura_contabilidade(COAVL node, char* id);
COAVL insert_contabilidade(COAVL node, char* id);
int foiVendidoAux(COAVL node,char** array,int elemento);
char* strdup(char*);
int getElementos_C(char* c[]);
void getAllElements_C(COAVL node,char** array);
COAVL insert_contabilidade2(COAVL node, char* id,float preco,int quantidade,int mes,char tipo);
int foiVendido(COAVL node,char** array,int size);
COAVL procura_contabilidadeN(COAVL node, char*id);
void contab_compras(COAVL node, char * cod_produto, int i, float array[]);
int totCompras_XtoY(COAVL node, int mes_inicial, int mes_final);
float totFacturacao_XtoY(COAVL node, int mes_inicial, int mes_final);
void calc_tot_compras(COAVL node, int array[]);
char** initArray(COAVL node[],int maxSize,int* size);

#endif

